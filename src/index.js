/** Created by Juan Manuel Ventura */

'use strict';

// don't want to require it on every file, you know, laziness...
global.m  = require('mithril');

var Root  = require('./root');

window.app = {
    m: m.mount(document.getElementById('app'), m.component(Root, {})),
    onDeviceReady: function(){
        console.log("Device is Ready!");
    }
}

document.addEventListener('deviceready', app.onDeviceReady, false);