/** Created by Juan Manuel Ventura */

'use strict';

//models
var Models = require('./models');

//components
  /** navigation **/
  var Navbar = require('./components/navigation/navBar');
  var Toolbar = require('./components/navigation/toolbar');
  /** views **/
  var Spots = require('./components/views/spots');
  var RendezVous = require('./components/views/rendezvous');


/**
 * This is the main component, all application logic must go here,
 * it passes any needed function to it's children component.
 * It also serves as the outer layout
 *
 * @module Root
 * @type {{controller: Function, view: Function}}
 */
module.exports = {
  controller: function (data) {
    // private, yeah!
    var items = Models.spots;
    var refresh = m.prop({ name: null, status: null });

    (function(){
      setAppScreen();
    })()

    /** functions **/
    function setAppScreen(){
      var appScreen = document.getElementById('app');
      appScreen.style.height = screen.height+'px';
      //appScreen.style.width = screen.width+'px';

    }
    function setViewsScreen(){
      var views = document.getElementsByClassName('views');
      if(views[0]) {
        views[0].style.height = (document.getElementById('app').offsetHeight - document.getElementById('toolbar').offsetHeight - document.getElementById('navBar').offsetHeight) + "px";
        views[1].style.height = views[0].style.height;

        //set google maps container
        document.getElementById('map-canvas').style.height = views[0].offsetHeight - document.getElementById('map-menu').offsetHeight + "px";
      }
    }

    return {
      /**
       * Gets all items
       * @returns {Array}
       * @example
       * var items = ctrl.getItems()
       */
      getItems: function () {
        return items;
      },
      setItems: function(data){
        items = data;
      },
      refresh: refresh,
      setAppScreen: setAppScreen,
      setViewsScreen: setViewsScreen
    }
  },

  view: function (ctrl) {
    function configViewsScreen( element, init, context ){
        if(!init)
          element.onload = ctrl.setViewsScreen();
    };

    // just mixin objects and values to make the point: you can pass anything
    return [
      m('div#toolbar',
          {},
          m.component(Toolbar, { refresh: ctrl.refresh })
      ),
      m('div#navBar',
        {},
        m.component(Navbar, {})
      ),
      m('div#spots', { config: configViewsScreen, class: "active col s12 views"},
        m.component(Spots, { spots: ctrl.getItems(), setItems: ctrl.setItems, refresh: ctrl.refresh })
      ),
      m('div#rendezvous',
          { class: "col s12 views" },
          m.component(RendezVous, {})
      )
    ];
  }
};