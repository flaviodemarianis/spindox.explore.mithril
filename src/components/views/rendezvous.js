/** Created by Juan Manuel Ventura */

'use strict';

/**
 * The header exposing the addition functionality consisting in an
 * input and a button. It expects the item collection to get passed
 * when initialized.
 *
 * @module Header
 * @type {{controller: Function, view: Function}}
 */
module.exports = {
  controller: function (opts) {

    // look ma, no `this` keyword!!!

    var input = m.prop('');
    //var items = opts.items;
    var items;

    return {
      input: input,
      items: items
    }

  },

  view: function (ctrl) {
    return m('div', 'rendez-vous view');
  }
};