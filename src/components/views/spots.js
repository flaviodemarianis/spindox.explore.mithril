/** Created by Juan Manuel Ventura */

'use strict';

var points = { data: m.prop([]), markers: m.prop([])};
var aList = m.prop([]), aListBox = m.prop('block');
var map = m.prop(null);
window.loadPoints = m.prop(null);

var List           = require('./../presentation/list');
var Autocomplete   = require('./../presentation/autocomplete');
var Map            = require('./../presentation/map');

/**
 * The header exposing the addition functionality consisting in an
 * input and a button. It expects the item collection to get passed
 * when initialized.
 *
 * @module Header
 * @type {{controller: Function, view: Function}}
 */
module.exports = {
  controller: function (opts) {

    var input = m.prop('');
    var items = opts.spots;

    /** constructor **/
    (function(){
    })();

    /** functions **/
    function addItem() {
      // empty item, a proper validation should go here
      if (!input()) return false;

      // the item is ok
      items.push({title: input()});

      // clean the input field
      input('');
      return true;
    };

    function searchItem(){

      input(document.getElementById('txtSearch').value);

      points.markers().forEach(function(m){
        if(input() === "") return false;

        if(m.title.toUpperCase().indexOf(input().toUpperCase()) > -1){
          console.log(m);
          map().setCenter(m.getPosition())
          map().setZoom(17);
        }
      });

    };

    function autocompleteItem(){
      aList([]);

      points.data().forEach(function(d){
        if(d.title.toUpperCase().indexOf(input().toUpperCase()) > -1){
          aList().push(d.title);
        }
      });

      if(aList().length > 0 && input().length > 2)
        aListBox('block');
      else
        aListBox('none');

      console.log(input(), aList(), aListBox());
    };

    /** returns **/
    return {
      input: input,
      spots: items,
      searchItem: searchItem,
      addItem: addItem,
      autocompleteItem: autocompleteItem,
      setPoints: function(d){ points.data(d.data); points.markers(d.markers); },
      setMaps: function(d){ map(d); },
      loadPoints: loadPoints,
      refresh: opts.refresh
    };

  },

  view: function (ctrl) {

    return m('div', [
                      m('div#map-menu', [
                        //m('button', {
                        //  onclick: ctrl.addItem.bind(ctrl /* just for being polite, not using `this` anyway */),
                        //  class: 'waves-effect waves-light btn',
                        //  style: 'width: 50%'
                        //}, 'add'),
                        m(".input-field.col.s12",  { style: 'margin-top: 0' },[
                          m(  "input.validate[id='txtSearch'][placeholder='Search the spot in map..'][type='text']",
                              {
                                oninput: m.withAttr('value', ctrl.input),
                                onkeyup: ctrl.autocompleteItem,
                                value: ctrl.input()
                              }
                          )
                        ]),
                        m('div', m.component(Autocomplete, { input: ctrl.input, data: aList, container: aListBox })),
                        m('button', {
                          onclick: ctrl.searchItem.bind(ctrl),
                          class: 'waves-effect waves-light btn teal lighten-3',
                          style: 'width: 100%'
                        }, 'search')
                      ]),
                      m( 'div#map-canvas', m.component(Map, { setPoints: ctrl.setPoints, setMaps: ctrl.setMaps, loadPoints: ctrl.loadPoints, refresh: ctrl.refresh }) )
                    ]
    );
  }
};