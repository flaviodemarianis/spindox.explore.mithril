/** Created by Juan Manuel Ventura */

'use strict';

/**
 * The list item component, it responsible only for rendering the
 * list items, it has no logic other than printing to the console
 * the clicked item for example proposes.
 *
 * @module ListItem
 * @type {{controller: Function, view: Function}}
 */
module.exports = {
  controller: function (data) {
    return {
      item:        data,

      /**
       * Prints out to the console the selected item
       * @param {String} item
       */
      setSelected: function (item) {
        console.log('selected:', item);
      }
    }
  },

  view: function (ctrl) {
    return m('li', {
      onclick: ctrl.setSelected.bind(ctrl, ctrl.item),
      class: 'item ' + ctrl.item.id_pos
    }, ctrl.item.title);
  }
};
