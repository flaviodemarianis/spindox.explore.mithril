'use strict';

module.exports = {
  controller: function(opts) {
    var items = opts.data;

    return {
      items: items,
      input: opts.input,
      container: opts.container
    }
  },

  view: function(ctrl){
    var aLi = [];

    ctrl.items().forEach(function(l){
      aLi.push(m('li', { onclick: function(){
                                    ctrl.container('none');
                                    ctrl.input(this.innerHTML);
                                  }
                       }, l));
    });

    return m('ul.autoList', { style: "display:"+ctrl.container()+";" }, aLi);
  }
};
