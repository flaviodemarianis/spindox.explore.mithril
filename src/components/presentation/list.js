/** Created by Juan Manuel Ventura */

'use strict';

var ListItem = require('./listItem');

/**
 * The list component, it renders listItems child components, for doing
 * so it expects to receive an item array when instantiated.
 * It is read only.
 *
 * @module List
 * @type {{controller: Function, view: Function}}
 */
module.exports = {
  controller: function (data) {
    return {
      items: data
    }
  },

  view: function (ctrl) {
    return m('ul', ctrl.items.map(function (item) {
      return m.component(ListItem, item);
    }));
  }
};
