'use strict';

var points = { data: [], markers: []};
var map = null, infowindow = null;
var currentPosition = null, watchId = null;


module.exports = {
  controller: function(opts){

      /** functions **/
      function setMaps(){
          var mapContainer = document.getElementById('map-canvas');

          if(mapContainer && !document.querySelector('#map-canvas .gm-style')) {
              var mapOptions = {
                  zoom: 5,
                  center: new google.maps.LatLng(42.11528356090148, 12.040484375000009),
                  zoomControl: false
              };
              map = new google.maps.Map(mapContainer, mapOptions);
              opts.setMaps(map);

              //creating infowindow for markers
              infowindow = new google.maps.InfoWindow({ maxWidth: 300 });

              loadPoints();

              //get current user position
              getCurrentPosition();

              /** event on maps **/
              google.maps.event.addListener(map, 'click', function(e) {
                  document.getElementById('txtSearch').blur();
                  infowindow.close();
              });
              google.maps.event.addListener(map, 'dragstart', function(e) {
                  document.getElementById('txtSearch').blur();
              });
              google.maps.event.addListener(map, 'zoom_changed', function(e) {
                  document.getElementById('txtSearch').blur();
              });

              //google.maps.event.trigger(map, 'resize');
          }

      };

      function loadPoints(){

          var xhrConfig = function(xhr) {
              xhr.setRequestHeader("Content-Type", "application/json");
              xhr.timeout = 15000;
              xhr.ontimeout = function () {
                  if(opts.refresh().name === "spots" && opts.refresh().status == 1) opts.refresh().status = -1;
                  alert("Connection error: timeout occurred.");
              }
          }

          //clear old objs data e markers un map
          if(points.data.length > 0) {
              points.data = []; //clean data
              points.markers.forEach(function(m){ //clean markers
                  m.setMap(null);
              });
              points.markers = []; //clean markers
          }

          m.request({
              method: 'GET',
              url: 'http://www.effelavionemecsek.com/2.0/component/com_geoSpots/ajax/getSpotsPosition.ajax.php',
              background: true, initialValue: [],
              config: xhrConfig
          })
          .then(function(data){
              if(data.err != 0) {
                  if(opts.refresh().name === "spots" && opts.refresh().status == 1) opts.refresh().status = -1;

                  return false;
              }

              data.pos.forEach(function(p){
                  points.data.push(p);
                  setMarker(p);
              });
              opts.setPoints(points);

              console.log(points);

              if(opts.refresh().name === "spots" && opts.refresh().status == 1) opts.refresh().status = 0;

              console.log(opts.refresh());

          })//.then(m.redraw);
      };

      function setMarker(pos){
          var marker = new google.maps.Marker({
              map: map,
              icon: "assets/images/icon_marker_explore.png",
              animation: google.maps.Animation.DROP,
              position: new google.maps.LatLng(parseFloat(pos.lat), parseFloat(pos.lng)),
              id_pos: pos.id_pos,
              codyoutube: pos.codyoutube,
              title: pos.title,
              user: pos.user,
              data_pos: pos.data_pos
          });
          points.markers.push(marker);

          //set description infowindow
          var descInfoW = "<div class='containerDescrizione' data-id_pos='"+pos.id_pos+"'>Spots Condiviso da <b>"+pos.user+"</b></div>";
          infowindow.setContent(descInfoW);

          /** Listener click on marker **/
          google.maps.event.addListener(marker, 'click', function(event){

              var that = this;
              var infoPosizione_address = "Indirizzo non disponibile.."; //default

              //get address position
              var geocoder = new google.maps.Geocoder();
              if (geocoder) {
                  geocoder.geocode({ latLng: new google.maps.LatLng(that.getPosition().lat(), that.getPosition().lng()) }, function (results, status) {
                      if (status == google.maps.GeocoderStatus.OK) {
                          //console.log(results[0].formatted_address);
                          infoPosizione_address = results[0].formatted_address;
                          var $descUser = $(infowindow.get('content')).filter('.containerDescrizione').html();
                          infowindow.setContent(
                              "<div class='containerDescrizione'>"+$descUser+"</div>"+
                              "<br>"+
                              "<div onclick=\"cordova.InAppBrowser.open('https://www.google.it/maps/@"+ that.getPosition().lat() +","+ that.getPosition().lng() +",17z', '_blank', 'location=yes')\" align='center' class='imgStreetView' data-lat='"+that.getPosition().lat()+"' data-lng='"+that.getPosition().lng()+"'><img src='http://maps.googleapis.com/maps/api/streetview?size=195x100&location="+that.getPosition().lat()+","+that.getPosition().lng()+"&heading=220&sensor=false' /></div>"+
                              "<div class='infoPosizione'>"+infoPosizione_address+"</div>"
                          );
                      }
                      else { console.log('No results found: ' + status); }
                  });
              }else console.log("error load geocoder");

              infowindow.open(map, that);

          });
          /* END - Listener click on marker */
      };

      function getCurrentPosition(loop){
          watchId = navigator.geolocation.watchPosition(
              function(pos){
                  console.log(pos);
                  currentPosition = pos;
                  map.setCenter(new google.maps.LatLng(currentPosition.coords.latitude, currentPosition.coords.longitude));
                  map.setZoom(11);

                  if(!loop)
                    navigator.geolocation.clearWatch(watchId);
              }, function(err){
                  console.log(err);
              }
          ,{ maximumAge: 5000, timeout: 20000, enableHighAccuracy: true });
      };

      return {
          getMaps: function(){ return {maps: map, mapsOptions: mapOptions, points: points}; },
          setMaps: setMaps,
          loadPoints: loadPoints,
          refresh: opts.refresh
      };
  },

  view: function(ctrl){
    function configMaps( element, init, context ){
      if(!init)
        element.onload = ctrl.setMaps();

    };

    //triggered refresh data in maps
    if(ctrl.refresh().name === "spots" && ctrl.refresh().status == 1) {
        console.log(ctrl.refresh());
        ctrl.loadPoints();
    }

    //return m('div', { onload: ctrl.setMaps, onclick: ctrl.setMaps }, 'loading maps..');
    return m( 'div', { config: configMaps} );

  }
};