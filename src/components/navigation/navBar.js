'use strict';

/**
 * Description
 *
 * @module navBar
 * @type {{controller: Function, view: Function}}
 */
module.exports = {
    controller: function (data) {
        return {}
    },

    view: function (ctrl) {
        return m(".row",
                    [
                        m(".col.s12", { style: "padding: 0;" }, [
                            m("ul.tabs", [
                                m("li.tab.col.s3", [m("a.active[href='#spots']", "Spots")]),
                                m("li.tab.col.s3", [m("a[href='#rendezvous']", "Rendez-vous")])
                            ])
                        ])
                    ]
                );
    }
};
