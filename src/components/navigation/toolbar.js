'use strict';

module.exports = {
    controller: function(opts){

        return { refresh: opts.refresh };
    },

    view: function(ctrl){
        return m("nav", [
                    m(".nav-wrapper", { class: "light-blue accent-3" },[
                        m("a.brand-logo[href='#!']", "Explore"),
                        m("ul.right", [
                            m("li.refresh", { onclick: function(){ ctrl.refresh({ name: "spots", status: 1 }) } } , [m("a", [m("i.mdi-navigation-refresh")])])
                        ])
                    ])
                ])
    }
};