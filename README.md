# README #

This is an example on how a mithril (> 0.20) project can be organized using components

**Intallation:**

```
#!bash

npm install
```

**Scripts**

```
#!bash
npm run build

```
builds the bundle


```
#!bash
npm run build:docs

```
builds the docs

```
#!bash
npm run watch

```
watches the working directory and builds the bundle on change

